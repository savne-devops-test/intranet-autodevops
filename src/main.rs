mod tests;

use tide::{Request, Response, StatusCode};
use tide::http::mime;

#[async_std::main]
async fn main() -> tide::Result<()> {
    let mut app = tide::new();
    app.at("/orders/shoes").get(order_shoes);
    app.at("/").get(welcome);
    app.listen("127.0.0.1:8080").await?;
    Ok(())
}

async fn order_shoes(mut _req: Request<()>) -> tide::Result {
    Ok(format!("Hello! I've put in an order for shoes").into())
}

async fn welcome(mut _req: Request<()>) -> tide::Result {
    let mut resp = Response::new(StatusCode::Ok);
    resp.set_body("<h1>Bienvenido</h1>");
    resp.set_content_type(mime::HTML);
    Ok(resp)
}